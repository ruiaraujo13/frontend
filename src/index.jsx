import React from 'react';
import ReactDOM from 'react-dom';
import {
	BrowserRouter,
	Route,
	Switch,
	Redirect
} from 'react-router-dom';
import {
	Provider
} from 'react-redux';
import configureStore from './store/config';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style/index.scss';

import Layout from './layouts';

const store = configureStore();

ReactDOM.render((
	<Provider store={store} >
		<BrowserRouter>
			<Switch>
				<Route
					store={store}
					path="/"
					name="React News"
					component={Layout}
				/>
				<Redirect from="*" to="/" />
			</Switch>
		</BrowserRouter>
	</Provider>
)  , document.getElementById('root'));
