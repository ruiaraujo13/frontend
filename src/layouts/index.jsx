import React, {
	Component
} from 'react';
import {
	Container
} from 'reactstrap';
import {
	Route,
	Switch,
	Redirect
} from 'react-router-dom';

import Header from '../components/Header';

import News from '../views/News'
import NewsDetails from '../views/NewsDetails';

export default class Layout extends Component {
	render() {
		return (
			<div className="App">
				<div className="App-container">
					<Header/>
					<main className="main">
					<Container fluid>
						<Switch>
							<Route
								path='/:id'
								name="News"
								component={NewsDetails}
							/>
							<Route
								path='/'
								name="News"
								component={News}
							/>
							<Redirect from="*" to="/" />
						</Switch>
						</Container>
					</main>
				</div>
			</div>
		);
	}
}
