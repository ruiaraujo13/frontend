import status, {
	GET_ALL,
	GET_ALL_SUCCESS,
	GET_ALL_ERROR,
	GET_ALL_CLEAR,
} from '../constants';

const initialState = {
	status: status.IDLE
};

const news = (state = initialState, action) => {
	switch (action.type) {
		case GET_ALL:
			return {
				...state,
				status: status.WORKING,
			};
		case GET_ALL_SUCCESS:
			return {
				...state,
				status: status.DONE,
				error: [],
				data: action.data
			};
		case GET_ALL_ERROR:
			return {
				...state,
				status: status.ERROR,
				error: action.error
			};
		case GET_ALL_CLEAR:
			return initialState;
		default:
			return state;
	}
};

export default news;
