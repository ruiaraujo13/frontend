import {
	GET_ALL,
	GET_ALL_SUCCESS,
	GET_ALL_ERROR,
	GET_ALL_CLEAR,
} from '../constants';

import axios from 'axios';

const fetchNews = () => {
	return {
		type: GET_ALL
	};
};

const fetchNewsSuccess = data => {
	return {
		type: GET_ALL_SUCCESS,
		data
	};
};

const fetchNewsError = error => {
	return {
		type: GET_ALL_ERROR,
		error
	};
};

export const getNews = (page, filter, orderBy, orderType) => {
	return async (dispatch, getState) => {
		try {
			dispatch(fetchNews());

			const reply = await axios.request({
				method: 'GET',
				url: `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_API_VERSION}/news`,
				params: {
					perPage: 9,
					page,
					filter,
					orderBy,
					orderType
				}
			});

			dispatch(fetchNewsSuccess(reply.data));
		} catch (err) {
			dispatch(fetchNewsError([err]));
		}
	};
};

export const clearNews = () => {
	return {
		type: GET_ALL_CLEAR
	};
}
