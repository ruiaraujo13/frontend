import {
	combineReducers
} from 'redux';

import news from './news/newsReducer';
import newsDetails from './newsDetails/newsDetailsReducer';

export default combineReducers({
	news,
	newsDetails
});
