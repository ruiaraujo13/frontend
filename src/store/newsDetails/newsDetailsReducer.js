import status, {
	GET_ONE,
	GET_ONE_SUCCESS,
	GET_ONE_ERROR,
	GET_ONE_CLEAR,
} from '../constants';

const initialState = {
	status: status.IDLE
};

const newsDetails = (state = initialState, action) => {
	switch (action.type) {
		case GET_ONE:
			return {
				...state,
				status: status.WORKING,
			};
		case GET_ONE_SUCCESS:
			return {
				...state,
				status: status.DONE,
				error: [],
				data: action.data
			};
		case GET_ONE_ERROR:
			return {
				...state,
				status: status.ERROR,
				error: action.error
			};
		case GET_ONE_CLEAR:
			return initialState;
		default:
			return state;
	}
};

export default newsDetails;
