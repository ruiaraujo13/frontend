import {
	GET_ONE,
	GET_ONE_SUCCESS,
	GET_ONE_ERROR,
	GET_ONE_CLEAR,
} from '../constants';

import axios from 'axios';

const fetchNews = () => {
	return {
		type: GET_ONE
	};
};

const fetchNewsSuccess = data => {
	return {
		type: GET_ONE_SUCCESS,
		data
	};
};

const fetchNewsError = error => {
	return {
		type: GET_ONE_ERROR,
		error
	};
};

export const getNews = id => {
	return async (dispatch, getState) => {
		try {
			dispatch(fetchNews());

			const reply = await axios.request({
				method: 'GET',
				url: `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_API_VERSION}/news/${id}`,
				params: {}
			});

			dispatch(fetchNewsSuccess(reply.data));
		} catch (err) {
			dispatch(fetchNewsError([err]));
		}
	};
};

export const clearNews = () => {
	return {
		type: GET_ONE_CLEAR
	};
}
