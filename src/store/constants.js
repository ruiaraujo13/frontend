export const GET_ALL = 'GET_ALL';
export const GET_ALL_SUCCESS = 'GET_ALL_SUCCESS';
export const GET_ALL_ERROR = 'GET_ALL_ERROR';
export const GET_ALL_CLEAR = 'GET_ALL_CLEAR';
export const GET_ONE = 'GET_ONE';
export const GET_ONE_SUCCESS = 'GET_ONE_SUCCESS';
export const GET_ONE_ERROR = 'GET_ONE_ERROR';
export const GET_ONE_CLEAR = 'GET_ONE_CLEAR';

export const status = {
	IDLE: 'IDLE',
	WORKING: 'WORKING',
	DONE: 'DONE',
	ERROR: 'ERROR'
};

export default status;
