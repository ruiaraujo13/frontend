import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../../components/Loading';
import Error from '../../components/Error';
import {
	getNews, clearNews
} from '../../store/newsDetails/newsDetailsActions';
import status from '../../store/constants';
import Details from '../../components/NewsDetails';

class NewsDetails extends Component {
	componentDidMount() {
		this.props.dispatch(getNews(this.props.match.params.id))
	}

	componentWillUnmount() {
		this.props.dispatch(clearNews());
	}

	render() {
		if (this.props.status === status.WORKING || this.props.status === status.IDLE) {
			return <Loading text={'Loading...'} />;
		}

		if (this.props.status === status.ERROR) {
			return <Error />;
		}

		if (this.props.status === status.DONE) {
			const { newsDetails } = this.props;

			return <Details newsDetails={newsDetails}/>
		}

		return null;
	}
}

NewsDetails.propTypes = {
	dispatch: PropTypes.func.isRequired,
	news: PropTypes.object
};

const mapStateToProps = state => {
	const {
		middleware, newsDetails
	} = state;
	return {
		middleware, newsDetails: newsDetails.data, status: newsDetails.status
	};
};

export default connect(mapStateToProps)(NewsDetails);

