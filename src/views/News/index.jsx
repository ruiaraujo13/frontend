import React, { Component, Fragment } from 'react';
import { Button } from 'reactstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../../components/Loading';
import Error from '../../components/Error';
import Grid from '../../components/NewsGrid';
import SearchAndSort from '../../components/NewsSearchSort';
import {
	getNews, clearNews
} from '../../store/news/newsActions';
import status from '../../store/constants';

class News extends Component {
	constructor(props) {
		super(props);

		this.state = {
			filter: '',
			orderBy: 'date',
			orderType: 'desc'
		}
	}

	componentDidMount() {
		this.props.dispatch(getNews())
	}

	componentWillUnmount() {
		this.props.dispatch(clearNews());
	}

	getMoreNews = () => {
		this.props.dispatch(getNews(this.props.news.currentPage + 1, null, this.state.orderBy, this.state.orderType))
	}

	onFilterChange = event => {
		this.setState({filter: event.target.value})
	}

	onSortChange = event => {
		const [ orderBy, orderType ] = event.target.value.split(':');

		this.setState({ orderBy, orderType }, () => {
			this.props.dispatch(getNews(null, null, orderBy, orderType));
		});
	}

	filterNews = () => {
		this.props.dispatch(getNews(null, this.state.filter, this.state.orderBy, this.state.orderType))
	}

	render() {
		if (this.props.status === status.WORKING || this.props.status === status.IDLE) {
			return <Loading text={'Loading Your Fresh News...'} />;
		}

		if (this.props.status === status.ERROR) {
			return <Error />;
		}

		if (this.props.status === status.DONE) {
			const {data, currentPage, lastPage} = this.props.news;

			return (
				<Fragment>
					<SearchAndSort
						filter={this.state.filter}
						onFilterChange={this.onFilterChange}
						filterNews={this.filterNews}
						onSortChange={this.onSortChange}
						orderBy={this.state.orderBy}
						orderType={this.state.orderType}
					/>
					<Grid news={data}></Grid>
					<hr/>
					{currentPage >= lastPage ?
						<p>No More News Today...</p>
						:
						<Button outline color="primary" className="mt-2 mb-4" onClick={this.getMoreNews}>Get More News...</Button>
					}
				</Fragment>
			);
		}

		return null;

	}
}

News.propTypes = {
	dispatch: PropTypes.func.isRequired,
	news: PropTypes.object
};

const mapStateToProps = state => {
	const {
		middleware, news
	} = state;
	return {
		middleware, news: news.data, status: news.status
	};
};

export default connect(mapStateToProps)(News);

