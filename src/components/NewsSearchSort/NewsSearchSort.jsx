import React, { Component } from 'react';
import { Button, Row, Col, InputGroup, InputGroupAddon, Input} from 'reactstrap';

export default class NewsSearchSort extends Component {
  render() {
	  const { filter, onFilterChange, filterNews, onSortChange, orderBy, orderType } = this.props;

	  return (
			<Row className="mb-4">
				<Col lg="8" md="8" sm="12">
					<InputGroup>
						<Input onChange={onFilterChange} value={filter} />
						<InputGroupAddon addonType="append">
							<Button outline color="primary" onClick={filterNews}>Search</Button>
						</InputGroupAddon>
					</InputGroup>
				</Col>
				<Col lg="4" md="4" sm="12">
					<InputGroup>
						<InputGroupAddon addonType="prepend">
							<Button outline color="link" disabled>Sort</Button>
						</InputGroupAddon>
						<Input type="select" name="select" id="orderSelect" value={`${orderBy}:${orderType}`} onChange={onSortChange}>
							<option value={`date:asc`}>Date - Oldest to New</option>
							<option value={`date:desc`}>Date - New to Oldest</option>
							<option value={`author:asc`}>Author - From A to Z</option>
							<option value={`author:desc`}>Author - From Z to A</option>
							<option value={`title:asc`}>Title - From A to Z</option>
							<option value={`title:desc`}>Title - From Z to A</option>
						</Input>
					</InputGroup>
				</Col>
			</Row>
    );
  }
}
