import React, { Component } from 'react';
import { Button, Jumbotron, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom'
import moment from 'moment';

export default class NewsDetails extends Component {
  render() {
	  const { newsDetails } = this.props;

	  return (
		<Jumbotron>
			<h4 className="display-3">{newsDetails.title}</h4>
			<hr className="my-2" />
			<Row>
				<Col lg="6" md="6" sm="12" className="mt-2 mb-2">
					<img src={newsDetails.image} alt={newsDetails.title} style={{width: '50%'}}/>
				</Col>
				<Col lg="6" md="6" sm="12" className="mt-2 mb-2">
					<p className="lead">{newsDetails.body}</p>
				</Col>
			</Row>
			<hr className="my-2" />
			<small className="text-muted">{moment(newsDetails.date).format('L')}| <i className="mr-1">By</i>{newsDetails.author}</small>
			<p className="lead mt-4 mb-2">
				<Link to={"/"}>
					<Button outline color="primary" className="mt-2 mb-4">Get Back...</Button>
				</Link>
			</p>
		</Jumbotron>
    );
  }
}
