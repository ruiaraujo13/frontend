import React, { Component } from 'react'

export default class Error extends Component {
	constructor(props) {
		super(props);
		this.state = {
			reloadColor: "primary"
		}
	}

	render() {
		return (
			<div>
				<p style={{
					textAlign: "center",
					fontSize: "1.5em"
				}}>Oh No! Something Went Wrong...</p>
				<p style={{
					textAlign: "center",
					color: this.state.reloadColor,
					cursor: "pointer"
				}} onClick={() => { window.location.reload() }} onMouseOver={() => this.setState({ reloadColor: "lightblue" })} onMouseOut={() => this.setState({ reloadColor: "#3399cc" })} target="_blank">Try Again.</p>
			</div>
		)
	}
}
