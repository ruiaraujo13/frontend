import React, { Component } from 'react';
import { Row, CardGroup } from 'reactstrap';
import NewsItem from './NewsItem';

export default class NewsGrid extends Component {
  render() {
	  const { news } = this.props;

	  return (
		<Row>
			<CardGroup>
				{news.map(newsItem => {
					return (<NewsItem key={newsItem.id} newsItem={newsItem}></NewsItem>);
				})}
			</CardGroup>
		</Row>
    );
  }
}
