import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Col, Card, CardBody, CardTitle, CardText, CardImg } from 'reactstrap';
import moment from 'moment';
import './NewsItem.scss'

export default class NewsItem extends Component {
  render() {
	  const { newsItem } = this.props;

	  return (
		<Col lg="4" md="6" sm="12" className="mt-2 mb-2">
			<Card className="h-100">
				<CardImg top width="100%" src={newsItem.image} alt={newsItem.name} />
				<CardBody>
					<CardTitle><h4>{newsItem.title}</h4></CardTitle>
					<CardText>{newsItem.body}...</CardText>
					<Link to={`/${newsItem.id}`}>Read More</Link>
					<CardText>
						<small className="text-muted">{moment(newsItem.date).format('L')}| <i className="mr-1">By</i>{newsItem.author}</small>
					</CardText>
				</CardBody>
			</Card>
		</Col>
    );
  }
}
