## Frontend

React/Redux Frontend

##### Run Locally
```
npm start
```

##### Run in Docker Container
```
docker build -t frontend .
docker run -d -p 3000:80 frontend
```
