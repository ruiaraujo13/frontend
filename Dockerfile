# build environment
FROM node:11.9.0-alpine as builder
WORKDIR /usr/src/app
COPY package.json /usr/src/app/package.json
RUN npm install
COPY . /usr/src/app
RUN npm run build

# production environment
FROM nginx:1.15.8-alpine
RUN mkdir -p /usr/share/nginx/html
WORKDIR /usr/share/nginx/html
COPY --from=builder /usr/src/app/build .
COPY --from=builder /usr/src/app/.env .
RUN chmod -R a+rx /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
